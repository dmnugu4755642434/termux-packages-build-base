#! /usr/bin/bash
export TERMUX_PACKAGES_BUILD_BASE_ROOT="$(pwd)"
export BUILD_TOOLS="$(lsb_release -c | awk '{print $2}')"
if [ "${BUILD_TOOLS}" == "xenial" ]; then
  export NDK="${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/ndk/20.0.5594570
else
  export NDK="${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/ndk/21.3.6528147
fi
