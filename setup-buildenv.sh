#! /usr/bin/bash
export TERMUX_PACKAGES_BUILD_BASE_ROOT="$(pwd)"
source "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/envsetup.sh
mkdir -p "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/cmdline-tools/latest
sudo apt update  >/dev/null 2>&1
sudo apt install wget unzip git -y >/dev/null 2>&1
wget https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip -O "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/commandlinetools-linux-7302050_latest.zip
unzip "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/commandlinetools-linux-7302050_latest.zip -d "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/
mv "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/cmdline-tools/* "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/cmdline-tools/latest/
rm -rf "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/commandlinetools-linux-7302050_latest.zip
"${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/cmdline-tools/latest/bin/sdkmanager --install "platforms;android-22" "build-tools;30.0.3"
if [ "${BUILD_TOOLS}" == "xenial" ]; then
  "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/cmdline-tools/latest/bin/sdkmanager --install "ndk;20.0.5594570"
else
  "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/cmdline-tools/latest/bin/sdkmanager --install "ndk;21.3.6528147"
fi
pushd "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"
git clone https://github.com/termux/termux-root-packages -b android-5
cd termux-root-packages
git submodule init
git submodule update
popd
mkdir -p "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/lib/android-sdk/platforms
ln -s "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/build-tools "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/lib/android-sdk/build-tools
ln -s "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/Android/Sdk/platforms/android-22 "${TERMUX_PACKAGES_BUILD_BASE_ROOT}"/lib/android-sdk/platforms/android-24
